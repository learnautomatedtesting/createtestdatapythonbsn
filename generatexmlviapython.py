from faker import Faker
import xml.etree.ElementTree as ET

fake = Faker('nl_NL')  # Dutch locale

def generate_dutch_citizen_data():
    citizen = ET.Element("citizen")

    # First name and last name
    name = ET.SubElement(citizen, "name")
    name.text = fake.name()

    # BSN
    bsn = ET.SubElement(citizen, "bsn")
    bsn.text = fake.ssn()  # In the Dutch locale, ssn() generates a BSN.

    return citizen

def main():
    root = ET.Element("citizens")

    # Generate data for 10 citizens
    for _ in range(10):
        citizen = generate_dutch_citizen_data()
        root.append(citizen)

    # Convert the XML data to a string and save
    tree = ET.ElementTree(root)
    tree.write("dutch_citizens_data.xml")

if __name__ == "__main__":
    main()